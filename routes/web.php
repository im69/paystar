<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AddressController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('order',[OrderController::class,'index'])->name('order');
Route::get('order/{order}',[OrderController::class,'show'])->name('order.details');
Route::get('/addresses/{order}', [AddressController::class, 'index'])->name('addresses.index');
Route::any('/pay/{order}', [OrderController::class, 'pay'])->name('pay');
Route::any('/verify', [OrderController::class, 'verify'])->name('user.payment_verify');
