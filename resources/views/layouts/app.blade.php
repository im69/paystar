<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content=" width=device-width, initial-scale=1"/>
    <!-- TITLE -->
    <title>paystar</title>
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.rtl.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">

</head>

<body>
@include('section.header')
<div class="container  mt-5">
    @yield('content')
</div>
</body>

</html>
