<div class="header">
    <header>
        <div class="container">
            <div class="d-flex">
                <div class="col-2">
                    <img width="50"  src="{{asset('images/logo.png')}}">
                </div>
                <div class="col-10 text-end">
                    <a href="{{route('order')}}">
                        <div  class="icon-cart">
                            <i class="fa fa-shopping-cart"></i>
                            <span  class="header-badge cart-items-count">سفارشات</span>
                        </div>
                    </a>

                </div>
            </div>
        </div>
    </header>
</div>
