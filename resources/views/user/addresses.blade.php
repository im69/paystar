@extends('layouts.app')
@section('content')
    @include('errors')
    <form method="post" action="{{route('pay',['order'=>$order])}}">
        @csrf
        <div class="row">
            <h3>انتخاب آدرس</h3>
            <hr>
            @foreach ($addresses as $address)

                <div class="col-12 mt-5">
                    <input {{$address->id == $order->address_id ? 'checked':''}} type="radio" name="address" value="{{$address->id}}">

                    <span class="mr-2"> عنوان آدرس : <span> {{ $address->title }}
                                                            </span> </span>
                    {{ $address->address }}
                    <br>
                    <span> استان : {{ $address-> province_name($address->province_id) }} </span>
                    <span> شهر : {{ $address->city_name($address->city_id) }} </span>
                    کدپستی :
                    {{ $address->postal_code }}
                    شماره موبایل :
                    {{ $address->cellphone }}

                </div>
            @endforeach

            <div class="mt-5">
                <span>شماره کارت</span>
                <input type="text" name="card_number">
            </div>
            <p class="mt-5"><b>مبلغ قابل پرداخت:</b><span>     {{ number_format($order->paying_amount) }}
                        تومان</span></p>
            <div class="row">
                <div class="col-12 text-end">
                    <button  class="btn btn-primary"> تایید نهایی</button>
                </div>
            </div>
        </div>
    </form>
@endsection
