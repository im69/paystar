@extends('layouts.app')
@section('content')
    <div class="row">
        <table class="table table-bordered table-responsive table-light">
            <thead>
            <tr>
                <th> نام محصول</th>
                <th> فی</th>
                <th> تعداد</th>
                <th> قیمت کل</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($order->orderItems as $item)
                <tr>
                    <td class="product-name"> {{ $item->product->name }}</td>
                    <td class="product-price-cart"><span class="amount">
                                                                {{ number_format($item->price) }}
                                                                تومان
                                                            </span></td>
                    <td class="product-quantity">
                        {{ $item->quantity }}
                    </td>
                    <td class="product-subtotal">
                        {{ number_format($item->price * $item->quantity) }}
                        تومان
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    @if($order->payment_status != 1)
        <div class="row">
            <div class="col-12 text-end">
                <a href="{{route('addresses.index',['order'=>$order])}}" class="btn btn-primary">ادامه خرید</a>
            </div>
        </div>
    @endif
@endsection

