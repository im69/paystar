@extends('layouts.app')
@section('content')
<table class="table table-responsive table-bordered table-light orders">
    <thead>
      <tr>
          <th>وضعیت</th>
          <th>تاریخ</th>
          <th>کد سفارش</th>
          <th>مبلغ</th>
      </tr>
    </thead>
    <tbody>
        @if($orders)
            @foreach($orders as $order)
                <tr onclick="window.location = '{{route('order.details',['order'=>$order])}}'">
                    <td>{{$order->payment_status}}</td>
                    <td>{{verta($order->created_at)->format('%d %B، %Y')}}</td>
                    <td>{{$order->id}}</td>
                    <td>
                        {{ number_format($order->paying_amount) }}
                        تومان
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>

</table>
@endsection

