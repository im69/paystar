<?php

namespace App\PaymentGateway;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Transaction;
use App\Models\ProductVariation;
use App\Notifications\PaymentReceipt;
use Illuminate\Support\Facades\DB;

class Payment
{
    public function createOrder($amounts, $res, $gateway_name)
    {
        try {
            DB::beginTransaction();

            Transaction::create([
                'user_id' => 1,
                'amount' => $amounts['paying_amount'],
                'token' => $res['token'],
                'order_id' => $res['order_id'],
                'ref_id' => $res['ref_num'],
                'gateway_name' => $gateway_name
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return ['error' => $ex->getMessage()];
        }

        return ['success' => 'success!'];
    }

    public function updateOrder($result)
    {
        try {
            DB::beginTransaction();

            $transaction = Transaction::where('ref_id', $result['ref_num'])->firstOrFail();
            $order = Order::where('id', $result['order_id'])->first();
            $status = 2;
            if ($order->card_number == $result->card_number) {
                $status = 1;
            }
            $order->update([
                'payment_status' => $status,
            ]);
            $transaction->update([
                'status' => $status,
            ]);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            return ['error' => $ex->getMessage()];
        }

        return ['success' => 'success!'];
    }
}
