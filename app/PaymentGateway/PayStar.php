<?php

namespace App\PaymentGateway;


class PayStar extends Payment
{
    public function send($order, $description)
    {
        $data = array(
            'amount' => $order['paying_amount'] *10,
            'callback' => route('user.payment_verify', ['order' => $order]),
            'callback_method' =>1,
            'description' => $description,
            'order_id' => $order['id']
        );

        $jsonData = json_encode($data);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://core.paystar.ir/api/pardakht/create',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_SSL_VERIFYHOST=>false,
            CURLOPT_SSL_VERIFYPEER=>false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer 0yovdk2l6e143',
                'Content-Type: application/json'
            ),
        ));

        $result = curl_exec($curl);
        $err = curl_error($curl);
        $result = json_decode($result, true);
        curl_close($curl);
     //   var_dump($result);exit;


        if ($err) {
            return ['error' => "cURL Error #:" . $err];
        } else {
            if ($result["status"] == 1) {

                $createOrder = parent::createOrder($order, $result["data"], 'paystar');
                if (array_key_exists('error', $createOrder)) {
                    return $createOrder;
                }

                return ['success' => 'https://core.paystar.ir/api/pardakht/payment?token=' . $result["data"]['token']];
            } else {
                return ['error' => 'ERR: ' . $result["status"]];
            }
        }
    }

    public function verify($result)
    {

        if ($result->status == 1) {
            $updateOrder = parent::updateOrder($result);
            if (array_key_exists('error', $updateOrder)) {
                return $updateOrder;
            }
            return ['success' => 'تراکنش موفق. شماره پیگیری:' . $result->ref_num];
        } else {
            switch ($result->status) {
                case "-1" :
                    $msg = "درخواست نامعتبر (خطا در پارامترهای ورودی).";
                    break;
                case "-2" :
                    $msg = "درگاه فعال نیست";
                    break;
                case "-3" :
                    $msg = "توکن تکراری است";
                    break;
                case "-4" :
                    $msg = "مبلغ بیشتر از سقف مجاز درگاه است";
                    break;
                case "-5" :
                    $msg = "شناسه ref_num معتبر نیست";
                    break;
                case "-6" :
                    $msg = "تراکنش قبلا وریفای شده است";
                    break;
                case "-7" :
                    $msg = "پارامترهای ارسال شده نامعتبر است";
                    break;
                case "-8" :
                    $msg = "تراکنش را نمیتوان وریفای کرد";
                    break;
                case "-9" :
                    $msg = "تراکنش وریفای نشد";
                    break;
                case "-98" :
                    $msg = "تراکنش ناموفق";
                    break;
                case "-99" :
                    $msg = "خطای سامانه";
                    break;
                default :
                    $msg = "خطای نامشخص";
                    break;

            }
            return ['error' => 'پیغام تراکنش:' . $msg];
        }
    }
}
