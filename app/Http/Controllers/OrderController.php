<?php

namespace App\Http\Controllers;



use App\Models\Order;
use App\PaymentGateway\PayStar;
use App\PaymentGateway\Zarinpal;
use Illuminate\Http\Request;

;

class OrderController extends Controller
{

    public function index(){
        $orders =  Order::where(['user_id'=>1])->get();

        return view('user.order',compact('orders'));
    }

    public function show(Order $order){
        return view('user.order_details',compact('order'));
    }
    public function pay(Order $order,Request $request){

        $request->validate([
            'card_number'=>'required|numeric|digits:16',
            'address'=>'required',
        ]);
        $order->update(['address_id'=>$request->address,'card_number'=>$request->card_number]);
        $payStarGateway = new PayStar();
        $payStarGatewayResult = $payStarGateway->send($order, 'پرداخت سفارش');
        return redirect()->to($payStarGatewayResult['success']);
    }

    public function verify(Request $request,Order $order){
        $payStarGateway = new PayStar();
        $payStarGatewayResult = $payStarGateway->verify($request);

        if (array_key_exists('error', $payStarGatewayResult)) {
            return view('user.verify',['msg'=>$payStarGatewayResult['error']]);
           // return redirect()->route('user.payment_verify',['msg'=>$payStarGatewayResult['error']]);
        } else {
            return view('user.verify',['msg'=>$payStarGatewayResult['success']]);
         //   return redirect()->route('user.payment_verify',['msg'=>$payStarGatewayResult['success']]);
        }
    }
}
