<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = "orders";
    protected $guarded = [];

    public function getPaymentTypeAttribute($paymentType)
    {
        switch($paymentType){
            case 'pos' :
                $paymentType = 'دستگاه pos';
                break;
            case 'online' :
                $paymentType = 'اینترنتی';
                break;
        }
        return $paymentType;
    }

    public function getPaymentStatusAttribute($paymentStatus)
    {
        switch($paymentStatus){
            case '0' :
                $paymentStatus = 'در انتظار پرداخت';
                break;
            case '1' :
                $paymentStatus = 'پرداخت شده';
                break;
            case '2' :
                $paymentStatus = 'پرداخت ناموفق';
                break;
        }
        return $paymentStatus;
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(UserAddress::class);
    }
}
