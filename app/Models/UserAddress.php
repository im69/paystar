<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;

    protected $table = "user_addresses";
    protected $guarded = [];

    function province_name($provinceId)
    {
        return Province::findOrFail($provinceId)->name;
    }

    function city_name($cityId)
    {
        return City::findOrFail($cityId)->name;
    }
}
